FROM yiisoftware/yii2-php:7.2-apache
COPY ./ /app
RUN composer install
RUN php init --env=Development --overwrite=y