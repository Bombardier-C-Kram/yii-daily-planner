<?php
return [
    'adminEmail' => 'holdenrosshoover@gmail.com',
    'supportEmail' => 'holdenrosshoover@gmail.com',
    'senderEmail' => 'holdenrosshoover@gmail.com',
    'senderName' => 'Holden Hover',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
];
