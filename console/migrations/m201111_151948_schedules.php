<?php

use yii\db\Schema;
use yii\db\Migration;

class m201111_151948_schedules extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%schedule}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'ownerid' => $this->smallInteger()->notNull(),
            'access' => $this->json(),
            'data' => $this->json(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%schedule}}');
    }
}