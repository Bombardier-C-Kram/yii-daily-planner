<?php

use yii\db\Migration;

/**
 * Class m201111_175835_init_rbac
 */
class m201111_175835_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        // add "createSchedule" permission
        $adminperm = $auth->createPermission('adminperm');
        $adminperm->description = 'Do Admin Stuff';
        $auth->add($adminperm);

        // add "createSchedule" permission
        $createSchedule = $auth->createPermission('createSchedule');
        $createSchedule->description = 'Create a Schedule';
        $auth->add($createSchedule);

        // add "updateSchedule" permission
        $updateSchedule = $auth->createPermission('updateSchedule');
        $updateSchedule->description = 'Update schedule';
        $auth->add($updateSchedule);

        // add "author" role and give this role the "createSchedule" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createSchedule);

        // add "admin" role and give this role the "updateSchedule" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateSchedule);
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $adminperm);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
}
