<?php
use common\controllers\DiscordLogTarget;

return [
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'          => DiscordLogTarget::class,
                    'webhookUrl'     => 'https://discord.com/api/webhooks/779087112925347840/6qrICMqREadp6f8Xil7kpZczaPSYmtHhsw-3ZOrTNEeK4vjge1--IRN0YGnjz1XAkzE1',
                    'levels'         => [ 'error' ],
                    'exportInterval' => 1,
                    'maskVars'       => [ '_SERVER', '_COOKIE' ],
                    'except'         => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:403',
                        'yii\i18n\*',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Hide index.php
            'showScriptName' => false,
            // Use pretty URLs
            'enablePrettyUrl' => true,
            'rules' => [
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=dailyplanner',
            'username' => 'dailyplanner',
            'password' => "secret",
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
