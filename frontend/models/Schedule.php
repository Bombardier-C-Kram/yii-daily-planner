<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property string $name
 * @property-read string $owner
 * @property string|null $access
 * @property string $data
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Schedule extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    const STATUS = [self::STATUS_DELETED => 'Deleted', self::STATUS_INACTIVE => 'Inactive', self::STATUS_ACTIVE => 'Active'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'ownerid', 'data'], 'required'],
            [['ownerid', 'status', 'created_at', 'updated_at'], 'number'],
            [['access', 'data'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => array_keys(self::STATUS)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ownerid' => 'OwnerID',
            'access' => 'Access',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At'
        ];
    }
}
