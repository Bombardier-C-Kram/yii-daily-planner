<footer class="main-footer">
    <!-- To the right -->

    <!-- Default to the left -->
    <strong>Copyright &copy <?= date("Y") ?> <a href="<?= \yii\helpers\BaseUrl::home() ?>"><?= Yii::$app->name ?></a>.</strong> All rights reserved.
    <div class="float-right d-none d-sm-inline">
        <img src="https://madewithlove.now.sh/ca" alt="Made with love in Canada">
        <img src="https://madewithlove.now.sh/se" alt="Made with love in Sweden">
        <strong>Version: <?= Yii::$app->version ?></strong>
    </div>
</footer>